FROM nginx:1.19.2-alpine

RUN apk add --no-cache \
    certbot \
    openssl

# Copy the base configuation
COPY ./src/config/nginx.conf /etc/nginx/nginx.conf
COPY ./src/config/.config ./.config

# Copy scripts and make them executable
# Directly in local/bin to have nice executable commands
COPY ./src/scripts /usr/local/bin/
RUN chmod +x /usr/local/bin/*

# Copy the example html page
# You can always be sure that this page will work
COPY ./src/config/index.html /var/www/app/index.html

# Set default value of environment variables
ENV STAGING=false

EXPOSE 80 443

CMD ["run.sh"]
