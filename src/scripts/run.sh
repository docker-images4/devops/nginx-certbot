#!/bin/sh

set -e

# Generate self-signed script to start
init_self_signed_cert

# Start nginx to get the certificates
nginx &

LETSENCRYPT_PATH=/etc/letsencrypt/live/

for config_file in $(ls /custom-nginx/)
do
  echo "Parsing $config_file"
  parse_user_config "/custom-nginx/$config_file"
done



nginx -s reload

wait_next_renewal