.PHONY: test_locally


#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
TEMP_NAME = temp
EMAIL = you.email@mail.com
STAGING = false

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Remove temporary docker
rm_temp:
	docker stop $(TEMP_NAME) || true
	docker rm $(TEMP_NAME) || true

## Test the build locally
test_run: rm_temp
	docker build -t $(TEMP_NAME) .
	docker run -it \
		-p 80:80 \
		-p 443:443 \
		-v "$(PROJECT_DIR)/data/letsencrypt:/etc/letsencrypt" \
		-v "$(PROJECT_DIR)/example/nginx.conf:/custom-nginx/1.conf" \
		-e EMAIL=${EMAIL} \
		-e STAGING=$(STAGING) \
		$(TEMP_NAME)

## Test the build and keep a prompt open (won't run the entrypoint/CMD)
test_build: rm_temp
	docker build -t $(TEMP_NAME) .
	docker run -it \
		-p 80:80 \
		-p 443:443 \
		-v "$(PROJECT_DIR)/data/letsencrypt:/etc/letsencrypt" \
		-v "$(PROJECT_DIR)/example/nginx.conf:/custom-nginx/1.conf" \
		-e EMAIL=${EMAIL} \
		-e STAGING=$(STAGING) \
		$(TEMP_NAME) sh

#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
